import {Component} from '@angular/core';
import {UserObjectInterface} from '../../interface/user.object.interface';
import {map} from "rxjs/operators";
import {Observable} from "rxjs/internal/Observable";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  userData: UserObjectInterface;

  constructor() {
    this.start();
  }

  start() {
    this.loginUser().then(
      (val) => {
        if (val !== null) {
          this.userData = val;
        }
      }
    );
  }

  loginUser(): any {
    return new Promise<UserObjectInterface>((resolve, reject) => {
      let userData: any = null;
      this.getUser().subscribe(data => userData = data);
      //this.userData = userData;
      resolve(userData);
    });
  }

  getUser(): Observable<UserObjectInterface> {
    return new Observable<UserObjectInterface>(
      observer => {
        observer.next({name: 'vasil', lastName: 'petro'});
      }
    )
  }
}
